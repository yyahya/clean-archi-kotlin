# Template API KATA CLEAN ARCHI TDD

Repository d'un template API pour des KATA afin de faire emerger des standards de code, clean arch, TDD, pattern microservice.   

## Prérequis

Vous devez avoir correctement installé les programmes suivants :

* [Git](https://git-scm.com/)
* [Java](https://www.oracle.com/java/technologies/downloads/) 

## Installation

```bash
$ git clone git@gitlab.com:yyahya/clean-archi-kotlin.git
$ cd kotlin-api/
```

## Développement

```bash
$ mvn clean install
$ java -jar kotlin-api-0.0.1-SNAPSHOT.jar
```

